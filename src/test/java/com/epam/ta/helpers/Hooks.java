package com.epam.ta.helpers;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class Hooks {

    @Autowired
    private BrowserstackUtils browserstackUtils;

    @Value("${browserName:chrome}")
    private String browserName;

    @After
    public void setSessionStatus(Scenario scenario) {
        if (browserName.equals("remote")) {
            browserstackUtils.setStatus(scenario);
        }
    }
}
