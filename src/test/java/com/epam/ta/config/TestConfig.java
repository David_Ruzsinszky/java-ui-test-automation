package com.epam.ta.config;

import com.epam.ta.webdriverfactory.WebDriverFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@ComponentScan("com.epam.ta")
public class TestConfig {
    public static final Duration WAIT_SECONDS = Duration.ofSeconds(20);
    public static final Duration POLLING_SECONDS = Duration.ofSeconds(1);

    @Bean(destroyMethod = "tearDown")
    public WebDriverFactory webDriverFactory() {
        return new WebDriverFactory();
    }
}
