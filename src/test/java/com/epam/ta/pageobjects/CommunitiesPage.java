package com.epam.ta.pageobjects;

import com.epam.ta.webdriverfactory.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

import java.util.*;

import static com.epam.ta.helpers.Addresses.*;
import static java.util.Map.entry;

@Component
public class CommunitiesPage extends CommonPageObject {

    public CommunitiesPage(WebDriverFactory factory) {
        super(factory);
    }

    @FindBy(css = "div[class='evnt-search-filter'] input[class*='evnt-search']")
    private static WebElement searchField;

    @FindBy(css = "div[class='evnt-community-card'] a[href$='idea-pool']")
    private static WebElement ideaPoolCard;

    @FindBy(css = "div[class='evnt-card-name'] h1")
    private static WebElement header;

    @FindBy(css = "div[class='evnt-articles-column']")
    private static List<WebElement> articlesColumnPath;

    @FindBy(css = "div[class='evnt-promo-cards-column cell-6'] div[class^='evnt-promo-card']")
    private static List<WebElement> pastEventsCard;

    private final Map<String, WebElement> webElementMap = Map.ofEntries(
            entry("Search", searchField)
            , entry("Idea Pool card", ideaPoolCard)
            , entry("Idea Pool page", header));

    private final Map<String, List<WebElement>> listOfElements = Map.ofEntries(
            entry("Articles panel", articlesColumnPath)
            , entry("Past events panel", pastEventsCard));

    public void fillTheField(String field, String input) {
        sendKeys(validateMap(webElementMap, field), input);
    }

    public void clickOnTheCard(String cardName) {
        waitAndClick(validateMap(webElementMap, cardName));
    }

    public String getElementText(String element) {
        WebElement webElement = validateMap(webElementMap, element);
        waitForElementToBeVisible(webElement);
        return getText(webElement);
    }

    public void clickOnTheButton(String button) {
        waitAndClick(validateMap(webElementMap, button));
    }

    public void navigateToCommunityPage() {
        navigateToUrl(COMMUNITIES_PAGE);
    }

    public int numberOfElements(String panel) {
        return getNumberOfElements(listOfElements, panel);
    }
}
