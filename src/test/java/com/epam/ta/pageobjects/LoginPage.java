package com.epam.ta.pageobjects;

import com.epam.ta.webdriverfactory.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

import java.util.Map;

import static java.util.Map.entry;

@Component
public class LoginPage extends CommonPageObject {

    @FindBy(id = "username")
    private static WebElement userEmail;

    @FindBy(id = "kc-login-next")
    private static WebElement continueButton;

    @FindBy(id = "firstName")
    private static WebElement firstName;

    @FindBy(id = "lastName")
    private static WebElement lastName;

    @FindBy(id = "password")
    private static WebElement password;

    public LoginPage(WebDriverFactory factory) {
        super(factory);
    }

    private final Map<String, WebElement> webElementMap = Map.ofEntries(
            entry("Email", userEmail)
            , entry("Continue", continueButton)
            , entry("First name", firstName)
            , entry("Last name", lastName)
            , entry("Password", password));

    public void fillTheField(String field, String input) {
        sendKeys(validateMap(webElementMap, field), input);
    }

    public void clickOnTheButton(String button) {
        waitAndClick(validateMap(webElementMap, button));
    }

    public String getFieldValue(String field) {
        waitForElementToBeVisible(webElementMap.get(field));
        return webElementMap.get(field).getAttribute("value");
    }

    public void checkTheErrorMessage(String error) {
        waitForElementToBeVisible(factory.getDriver().findElement(By.xpath(String.format("//li[text()=\"%s\"]", error))));
    }
}
