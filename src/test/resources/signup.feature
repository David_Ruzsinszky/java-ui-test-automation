Feature: Sign Up functionality

  Background: Navigate to the Login screen
    Given the 'Main' site is opened
    And the 'Login' button is clicked on the 'Main' page
    When the 'Email' field is filled with 'example.email@email.com' on the 'Login' page
    And the 'Continue' button is clicked on the 'Login' page

  Scenario: User credentials are autofilled
    Then the 'First name' and 'Last name' fields should be autofilled with the followings
      | firstName | lastName |
      | example   | email    |

  Scenario Outline: Sign up with invalid password
    When the '<field>' is filled with '<value>'
    Then the '<errorMessage>' error message of the '<field>' field should be shown

    Examples:
      | field    | value        | errorMessage                  |
      | Password | test         | at least 9 characters         |
      | Password | test12345    | at least 1 capital letter     |
      | Password | passwordtest | at least 1 special characters |