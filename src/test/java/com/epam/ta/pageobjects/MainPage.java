package com.epam.ta.pageobjects;

import com.epam.ta.webdriverfactory.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.epam.ta.helpers.Addresses.*;
import static java.util.Map.entry;

@Component
public class MainPage extends CommonPageObject {

    @FindBy(css = "li[class='nav-item communities-icon'] a[class='nav-link']")
    private static WebElement communitiesButton;

    @FindBy(css = "li[class='evnt-header-button login'] a")
    private static WebElement loginButton;

    private final Map<String, WebElement> webElementMap = Map.ofEntries(entry("Communities", communitiesButton), entry("Login", loginButton));

    public MainPage(final WebDriverFactory factory) {
        super(factory);
    }

    public void navigateToMainPage() {
        navigateToUrl(MAIN_PAGE);
    }

    public void clickOnTheButton(String button) {
        waitAndClick(validateMap(webElementMap, button));
    }
}
