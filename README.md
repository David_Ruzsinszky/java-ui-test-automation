# Java UI Test Automation

## Description
Java UI automation project for educational purposes.

## Used tools
- Java 17
- Maven 3.8.6

## Run tests

- To run scenarios the following command should be used in the terminal: '_mvn test_'
- To run scenarios in headless mode use the following command: '_mvn test -Dheadless=true_'
- To run scenarios in selected browser in custom resolution use this command: '_mvn verify -DbrowserName=chrome
  -Dwidth=1280 -Dheight=1024_'
- Supported browsers:
  - chrome
  - firefox
  - safari
- To run scenarios with Browserstack:
  - 1, Save your Browserstack credentials as environment variables
    'BROWSERSTACK_USERNAME=YOU_USERNAME'
    'BROWSERSTACK_ACCESS_KEY=YOUR_KEY'
  - 2, Use this command to run the scenarios: mvn verify -DbrowserName=remote -Premote
  - 3, To generate your custom yml find more details
    here: [https://www.browserstack.com/docs/automate/selenium/sdk-config-generator]