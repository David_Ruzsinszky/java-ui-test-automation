package com.epam.ta.helpers;

import com.epam.ta.webdriverfactory.WebDriverFactory;
import io.cucumber.java.Scenario;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class BrowserstackUtils {

    @Autowired
    private WebDriverFactory webDriverFactory;

    private final ArrayList<String> statuses = new ArrayList<>();


    public void setStatus(Scenario scenario) {
        var driver = webDriverFactory.getDriver();
        statuses.add(String.valueOf(scenario.getStatus()));

        if (statuses.contains("FAILED")) {
            setSessionStatus(driver, "failed", String.format("Scenario: %s is failed.", scenario.getName()));
        } else {
            setSessionStatus(driver, "passed", String.format("Scenario: %s is passed.", scenario.getName()));
        }
    }

    private void setSessionStatus(WebDriver driver, String status, String reason) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript(String.format("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": \"%s\", \"reason\": \"%s\"}}", status, reason));
    }
}
