Feature: Search functionality

  Background: : Home page is loaded properly
    Given the 'Main' site is opened

  Scenario: Search for the Idea Pool community
    Given the 'Communities' button is clicked on the 'Main' page
    And the 'Search' field is filled with 'Idea Pool' on the 'Community' page
    Then the 'Idea Pool card' is clicked
    Then the 'Idea Pool page' header should be 'Idea Pool'
    Then the 'Articles panel' contains at least 4 cards
    Then the 'Past events panel' contains at least 4 cards