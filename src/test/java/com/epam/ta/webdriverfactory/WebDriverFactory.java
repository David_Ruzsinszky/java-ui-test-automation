package com.epam.ta.webdriverfactory;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Objects;

@Component
public class WebDriverFactory {
    private WebDriver driver;

    @Value("${headless:false}")
    private Boolean headless;

    @Value("${browserName:chrome}")
    private String browserName;

    @Value("${width:1920}")
    private int width;

    @Value("${height:1080}")
    private int height;

    public WebDriver getDriver() {
        if (Objects.isNull(driver)) {
            driver = setUpWebDriver();
        }
        return driver;
    }

    private WebDriver setUpWebDriver() {
        WebDriver driver;
        {
            switch (browserName) {
                case "chrome" -> {
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver(new ChromeOptions().setHeadless(headless));
                }
                case "firefox" -> {
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver(new FirefoxOptions().setHeadless(headless));
                }
                case "safari" -> {
                    if (!headless) {
                        WebDriverManager.safaridriver().setup();
                        driver = new SafariDriver();
                    } else throw new RuntimeException("Safari browser does not support headless mode");
                }
                case "remote" -> {
                    try {
                        MutableCapabilities capabilities = new MutableCapabilities();
                        HashMap<String, Object> bstackOptionsMap = new HashMap<>();
                        bstackOptionsMap.put("source", "java-ui-test:sample-main:v1.0");
                        bstackOptionsMap.putIfAbsent("userName", System.getenv("BROWSERSTACK_USERNAME"));
                        bstackOptionsMap.putIfAbsent("accessKey", System.getenv("BROWSERSTACK_ACCESS_KEY"));
                        capabilities.setCapability("bstack:options", bstackOptionsMap);

                        driver = new RemoteWebDriver(new URL("https://hub.browserstack.com/wd/hub"), capabilities);
                    } catch (MalformedURLException e) {
                        throw new RuntimeException(e);
                    }
                }
                default ->
                        throw new RuntimeException(String.format("The %s as provided browser parameter is not valid", browserName));
            }
            driver.manage().window().setSize(new Dimension(width, height));
            return driver;
        }

    }

    public void tearDown() {
        if (Objects.nonNull(driver)) {
            driver.quit();
            driver = null;
        }
    }

}
