package com.epam.ta.stepdefinitions;

import com.epam.ta.pageobjects.CommunitiesPage;
import com.epam.ta.pageobjects.LoginPage;
import com.epam.ta.pageobjects.MainPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public class StepDefinitions {
    @Autowired
    MainPage mainPage;

    @Autowired
    CommunitiesPage communitiesPage;

    @Autowired
    LoginPage loginPage;

    @Given("the {string} site is opened")
    public void theSiteIsOpened(String site) {
        switch (site) {
            case "Main" -> mainPage.navigateToMainPage();
            case "Community" -> communitiesPage.navigateToCommunityPage();
        }
    }

    @And("the {string} field is filled with {string} on the {string} page")
    public void theFieldIsFilledWithInput(String field, String input, String page) {
        switch (page) {
            case "Community" -> communitiesPage.fillTheField(field, input);
            case "Login" -> loginPage.fillTheField(field, input);
        }
    }

    @And("the {string} and {string} fields should be autofilled with the followings")
    public void theUserCredentialsShouldBeTheFollowings(String firstName, String lastName, DataTable dataTable) {
        List<Map<String, String>> userCredentials = dataTable.asMaps(String.class, String.class);
        Assert.assertEquals(userCredentials.get(0).get("firstName"), loginPage.getFieldValue(firstName));
        Assert.assertEquals(userCredentials.get(0).get("lastName"), loginPage.getFieldValue(lastName));
    }

    @And("the {string} is filled with {string}")
    public void theFieldIsFilledWithValue(String field, String input) {
        loginPage.fillTheField(field, input);
    }

    @Then("^the \\'(.*)\\' error message of the '(?:.*)' (?:field) should be shown$")
    public void theErrorMessageOfTheFieldShouldBeShown(final String error) {
        loginPage.checkTheErrorMessage(error);
    }

    @Then("the {string} is clicked")
    public void theCardIsOnlyAvailable(String cardName) {
        communitiesPage.clickOnTheCard(cardName);
    }

    @Then("the {string} header should be {string}")
    public void theHeaderShouldBeTheExpected(String element, String header) {
        Assert.assertEquals(header, communitiesPage.getElementText(element));
    }

    @Then("the {string} contains at least {int} cards")
    public void thePanelContainsAtLeastNumberOfCards(String panel, int num) {
        Assert.assertEquals(communitiesPage.numberOfElements(panel), num);
    }

    @Given("the {string} button is clicked on the {string} page")
    public void theButtonIsClicked(String button, String page) {
        switch (page) {
            case "Main" -> mainPage.clickOnTheButton(button);
            case "Community" -> communitiesPage.clickOnTheButton(button);
            case "Login" -> loginPage.clickOnTheButton(button);
        }
    }
}
