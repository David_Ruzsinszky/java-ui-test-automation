package com.epam.ta.pageobjects;

import com.epam.ta.webdriverfactory.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.epam.ta.config.TestConfig.POLLING_SECONDS;
import static com.epam.ta.config.TestConfig.WAIT_SECONDS;

public abstract class CommonPageObject {
    WebDriverFactory factory;

    public CommonPageObject(WebDriverFactory factory) {
        PageFactory.initElements(factory.getDriver(), this);
        this.factory = factory;
    }

    public void waitAndClick(WebElement webElement) {
        setupBaseWait("clickable", webElement)
                .until(ExpectedConditions.elementToBeClickable(webElement)).click();
    }

    public void waitForElementToBeVisible(final WebElement webElement) {
        setupBaseWait("visible", webElement)
                .until(ExpectedConditions.visibilityOf(webElement));
    }

    public void sendKeys(WebElement webElement, String input) {
        waitAndClick(webElement);
        webElement.click();
        webElement.sendKeys(input);
    }

    protected void navigateToUrl(final String url) {
        factory.getDriver().get(url);
    }

    public WebElement validateMap(Map<String, WebElement> webElementMap, String elementName) {
        return Optional.ofNullable(webElementMap.get(elementName)).orElseThrow(() -> new RuntimeException(String.format("The provided argument is not valid. Valid arguments are: %s",
                webElementMap.keySet())));
    }

    public int getNumberOfElements(Map<String, List<WebElement>> listOfElements, String element) {
        return validateMapOfLists(listOfElements, element).size();
    }

    public String getText(WebElement element) {
        return element.getText();
    }

    private FluentWait<WebDriver> setupBaseWait(String interaction, WebElement webElement) {
        return new WebDriverWait(factory.getDriver(), WAIT_SECONDS)
                .pollingEvery(POLLING_SECONDS)
                .withMessage(String.format("The following element is not %s: %s", interaction, webElement));
    }

    private List<WebElement> validateMapOfLists(Map<String, List<WebElement>> webElementMap, String elementName) {
        return Optional.ofNullable(webElementMap.get(elementName)).orElseThrow(() -> new RuntimeException(String.format("The provided argument is not valid. Valid arguments are: %s",
                webElementMap.keySet())));
    }
}
